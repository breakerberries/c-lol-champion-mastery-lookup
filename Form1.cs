﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using RiotNet;

namespace RITOTOT
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //Config API Wrapper
            RiotClient.DefaultPlatformId = RiotNet.Models.PlatformId.EUW1;
            RiotClient.DefaultSettings = () => new RiotClientSettings
            {
                ApiKey = "RGAPI-913870aa-0797-4093-bb98-fedd29d2f2c0",
                ThrowOnError = true,
                ThrowOnNotFound = true
            };
            RR = new RiotClient();
            RR.ResourceNotFound += (s, e) => {MessageBox.Show("Not found!"); };

            //Chest & Mastery-Icon auf Champ-Bild
            pictureBox2.Parent = pictureBox1;
            pictureBox2.BackColor = Color.Transparent;
            pictureBox2.Location = new Point(pictureBox1.Width-pictureBox2.Width,0);
            pictureBox3.Parent = pictureBox1;
            pictureBox3.BackColor = Color.Transparent;
            pictureBox3.Location = new Point(pictureBox1.Width / 2 - pictureBox3.Width / 2, pictureBox1.Height - pictureBox3.Height);
            textBox1.Text = RITOTOT.Properties.Settings.Default.LastSum;
        }

        ////Initialisierung Public-Objekte////
        //RiotAPI
        IRiotClient RR;
        //Mastery-Tablle
        DataTable dt;
        //ChampionID <-> ChampionName Dictionary
        Dictionary<string,string> CDick;
        //DataTable fertig gerendert
        bool DTready = false;
        //Ursprungshöhe von Form
        int rightsize;

        private void Form1_Load(object sender, EventArgs e)
        {
            //Dictionary laden
            StoD();
            //Ursprungshöhe der Form speichern
            rightsize = this.Height;
            //Form verkleinern
            this.Height = 80;
        }


        //Liste zu DataTable konvertieren; SOURCE: https://stackoverflow.com/a/21047196
        private static DataTable ConvertToDT<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Abfrage: Gültiger Username
            if (textBox1.Text.Length > 2 && textBox1.Text.Length < 17)
            {
                //Vorbereitung
                DTready = false;
                dt = null;
                this.Height = 80;
                RiotNet.Models.Summoner summ;
                List<RiotNet.Models.ChampionMastery> aaR;
                label7.Text = ("Kontaktiere Riot-Server...Bitte warten!");
                try
                {
                    //Summoner suchen
                    summ = RR.GetSummonerBySummonerNameAsync(textBox1.Text).Result;
                    label7.Text = summ.Name + " (" + summ.Id + ")";
                    //Masteryliste speichern
                    aaR = RR.GetChampionMasteriesAsync(summ.Id).Result;
                }
                catch (Exception es)
                {
                    label7.Text = "ERROR!";
                    return;
                }

                //Letzten Summoner speichern
                RITOTOT.Properties.Settings.Default.LastSum = summ.Name;
                RITOTOT.Properties.Settings.Default.Save();

                //Wenn Liste leer
                if (aaR.Count <= 0)
                    {
                        label7.Text = "Keine Daten von "+ summ.Name + " (" + summ.Id + ")";
                        return;
                    }
                    dt = ConvertToDT(aaR);
                    //DataTable in View laden
                    dataGridView1.DataSource = dt;
                    //Spalten anpassen
                    dt.Columns.Add("CName");
                    dt.Columns.Add("Level");
                    dt.Columns.Add("Rank");
                    
                    dataGridView1.Columns["PlayerId"].Visible = false;
                    dataGridView1.Columns["DatabaseId"].Visible = false;
                    dataGridView1.Columns["ChestGranted"].Visible = false;
                    dataGridView1.Columns["TokensEarned"].Visible = false;
                    dataGridView1.Columns["ChampionId"].Visible = false;
                    dataGridView1.Columns["ChampionPointsSinceLastLevel"].Visible = false;
                    dataGridView1.Columns["PlayerId"].Visible = false;
                    dataGridView1.Columns["ChampionLevel"].Visible = false;
                    dataGridView1.Columns["Rank"].Visible = false;

                    dataGridView1.Columns["CName"].HeaderText = "Champion";                   
                    dataGridView1.Columns["ChampionPoints"].HeaderText = "Points";
                    dataGridView1.Columns["ChampionPointsUntilNextLevel"].HeaderText = "Next LevelUp";
                    dataGridView1.Columns["ChampionLevel"].HeaderText = "Level";
                    dataGridView1.Columns["LastPlayTime"].HeaderText = "Last Game (UTC)";
                    dataGridView1.Columns["Level"].DisplayIndex = 0;
                    dataGridView1.Columns["CName"].DisplayIndex = 0;
                    

                    //Platzhalter-String
                    string valle;
                    
                    //ChampionID übersetzen + Rank + Tokens
                    foreach (DataRow row in dt.Rows)
                    {
                        String boom = row[dt.Columns.IndexOf("ChampionId")].ToString();
                        if (CDick.TryGetValue(boom, out valle))
                        {
                            //CName
                            row[dt.Columns.IndexOf("CName")] = CDick[boom];

                            //Level+Tokens

                            row[dt.Columns.IndexOf("Level")] = row[dt.Columns.IndexOf("ChampionLevel")].ToString(); 
                            if (int.Parse(row[dt.Columns.IndexOf("ChampionLevel")].ToString()) > 4)
                                row[dt.Columns.IndexOf("Level")] += "." + row[dt.Columns.IndexOf("TokensEarned")].ToString();
                            //Rank
                            double xx = 0;
                            double xx2 = 0;

                            double.TryParse(row[dt.Columns.IndexOf("ChampionLevel")].ToString(), out xx2);
                            xx = xx2;
                            double.TryParse(row[dt.Columns.IndexOf("TokensEarned")].ToString(), out xx2);
                            xx = xx + (xx2 / 10);
                            double.TryParse(row[dt.Columns.IndexOf("ChampionPoints")].ToString(), out xx2);
                            xx += (xx2 / 10000000000);
                            row[dt.Columns.IndexOf("Rank")] = xx;
                        
                        }
                        //Wenn Champion nicht in Dictionary: Lade neue Liste
                        else
                        {
                            dataGridView1.DataSource = null;
                            DialogResult r = MessageBox.Show("Keine oder veraltete Championsliste vorhanden. Neue Liste laden?", "Error: Champion nicht gefunden!", MessageBoxButtons.OKCancel);
                            if(r==DialogResult.OK)
                            {
                                button1_Click(sender, e);
                                button3_Click(sender, e);
                            }                            
                            return;
                        }                      
                    }
                                        
                    //Oberfläche anpassen
                    DTready = true;
                    DataGridViewRow roww = dataGridView1.CurrentRow;
                    dataGridView1_CellClick(sender, null);
                    dataGridView1.Focus();
                    this.Height = rightsize;
                    dataGridView1.Sort(dataGridView1.Columns["Rank"],ListSortDirection.Descending);
                dataGridView1.AutoResizeColumns();
            }
            else
                MessageBox.Show("username error");
        }

        public class Chammp
        {
            public class Namme
            {
                public string id;
                public string title;
                public string name;
                public string key;
            }
        }

        //Championliste herunterladen und als String in Properties speichern. Danach Dictionary neuladen.
        public void button1_Click(object sender, EventArgs e)
        {
            //Dictionary<string, RiotCaller.StaticEndPoints.Champion.ChampionData>.ValueCollection asa = RC.staticApi.GetChampions(RiotCaller.Enums.region.euw, RiotCaller.Enums.language.en_US).Data.Values;
            Dictionary<string, RiotNet.Models.StaticChampion> asaR = RR.GetStaticChampionsAsync(tags: new[] { "all" }).Result.Data;
            String[] iaia = new String[asaR.Count];
            String[] aiai = new String[iaia.Length];
            Properties.Settings.Default.ChampDICid = "";
            Properties.Settings.Default.ChampDICname = "";
            int aaaa = 0;
            foreach (RiotNet.Models.StaticChampion a in asaR.Values)
            {
                iaia[aaaa] = a.Id.ToString();
                aiai[aaaa] = a.Name.ToString();
                RITOTOT.Properties.Settings.Default.ChampDICid += iaia[aaaa] + ";";
                RITOTOT.Properties.Settings.Default.ChampDICname += aiai[aaaa] + ";";
                aaaa++;
            }
            RITOTOT.Properties.Settings.Default.Save();
            MessageBox.Show("Neue Championliste gespiechert!");
            StoD();
            
        }

        //Gespeicherte Championliste von String zu Dictionary
        private void StoD()
        {
            String[] iaia = Properties.Settings.Default.ChampDICid.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            String[] aiai = Properties.Settings.Default.ChampDICname.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            CDick = new Dictionary<string, string>();
            for (int i = 0; i < iaia.Length; i++)
            {
                CDick.Add(iaia[i], aiai[i]);
            }
        }

        //Auswahl in DataGridView
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //DataRow initialisieren
            DataGridViewRow roww;
            if (e == null || e.RowIndex < 0)
            {
               roww = dataGridView1.CurrentRow;
            }
            else
            {
                roww = dataGridView1.Rows[e.RowIndex];
            }

            //Level-ProgressBar
            progressBar1.Value = 0;
            int clevel = (int)roww.Cells["ChampionLevel"].Value;
            label2.Visible = true;
            label3.Visible = true;
            label2.Text = clevel.ToString();
            label3.Text = (clevel+1).ToString();                     
            switch (clevel)
            {
                //Level5
                case 5:
                    progressBar1.Maximum = 20000;
                    progressBar1.Value = (int)roww.Cells["TokensEarned"].Value * 10000;
                    label5.Text = roww.Cells["TokensEarned"].Value.ToString() + "/" + 2 + " Tokens";
                    break;
                //Level6
                case 6:
                    progressBar1.Maximum = 20000;
                    progressBar1.Value = (int)roww.Cells["TokensEarned"].Value * 20000 / 3;
                    label5.Text = roww.Cells["TokensEarned"].Value.ToString() + "/" + 3 + " Tokens";
                    break;
                //Level7
                case 7:
                    progressBar1.Maximum = 20000;
                    progressBar1.Value = progressBar1.Maximum;
                    label5.Text = "Max. Level";
                    label2.Visible = false;
                    label3.Visible = false;
                    break;
                //Level1-4
                default:
                    progressBar1.Maximum = (int)(long)roww.Cells["ChampionPointsSinceLastLevel"].Value + (int)(long)roww.Cells["ChampionPointsUntilNextLevel"].Value;
                    progressBar1.Value = (int)(long)roww.Cells["ChampionPointsSinceLastLevel"].Value;
                    label5.Text = (int)(long)roww.Cells["ChampionPointsSinceLastLevel"].Value + "/" + progressBar1.Maximum + " pts";
                    break;
            }

            //Sammel-ProgressBar
            progressBar2.Value = 0;
            progressBar2.Maximum = 21600;
            if ((int)roww.Cells["ChampionPoints"].Value > 21600)
                progressBar2.Maximum = (int)roww.Cells["ChampionPoints"].Value;
            progressBar2.Value = (int)roww.Cells["ChampionPoints"].Value;

            //String Filling
            label1.Text = roww.Cells["CName"].Value.ToString();
            label4.Text = String.Format("{0:n0}", roww.Cells["ChampionPoints"].Value);
            label6.Text = "Level " + roww.Cells["ChampionLevel"].Value.ToString();
            label10.Text = "Last Game: " + roww.Cells["LastPlayTime"].Value.ToString()+" (UTC)";

            ////Bild laden////
            //HTTP-Pfad Champion
            StringBuilder sb = new StringBuilder();
            foreach (char c in label1.Text)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    sb.Append(c);
                }
            }

            //Ausnahmen in Champnamen
            String ccnn = sb.ToString();
            switch (ccnn)
            {
                case "KhaZix":
                    ccnn = "Khazix";
                    break;
                case "LeBlanc":
                    ccnn = "Leblanc";
                    break;
                case "VelKoz":
                    ccnn = "Velkoz";
                    break;
                case "ChoGath":
                    ccnn = "Chogath";
                    break;
                case "Wukong":
                    ccnn = "MonkeyKing";
                    break;
                default:
                     break;
            }

            pictureBox1.ImageLocation = "http://ddragon.leagueoflegends.com/cdn/img/champion/loading/" + ccnn + "_0.jpg";           
            //Chest
            if ((bool)roww.Cells["ChestGranted"].Value)
                pictureBox2.Visible = true;
            else
                pictureBox2.Visible = false;
            //Mastery
            pictureBox3.Image = (System.Drawing.Bitmap)Properties.Resources.ResourceManager.GetObject("Champ_Mastery_" + roww.Cells["ChampionLevel"].Value.ToString());
        }

        //TextBox-Enter aktivieren
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                button3_Click(sender, null);
        }

        //Bugfix: Falsche Reihe
        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if(DTready)
            dataGridView1_CellClick(sender, e);
        }

        //Bugfix: Falsche Reihe
        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dataGridView1_CellClick(sender, null);
        }

        //DEBUG: Höhe switchen
        private void label7_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.Height == 80)
                this.Height = rightsize;
            else
                this.Height = 80;
        }

        //Messagebox vor Click-Void
        private void button1_Click2(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Soll die Championliste erneuert werden?", "Listupdate", MessageBoxButtons.YesNoCancel);
            if (r == DialogResult.Yes)
                button1_Click(sender, e);
            else if (r == DialogResult.Cancel)
                ExcelBaby();
        }

        //DataTable zu CSV
        private void ExcelBaby()
        {
            if (dt == null || dt.Rows.Count <= 0)
                return;

            StringBuilder sb = new StringBuilder();

            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName);
            sb.AppendLine(string.Join(";", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(";", fields));
            }

            File.WriteAllText(label7.Text.Substring(0, label7.Text.IndexOf('(')-1)+"_"+System.DateTime.Today.ToString("ddMMyyhhmm")+".csv", sb.ToString());
        }
    }
}
